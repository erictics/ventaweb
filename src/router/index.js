import Vue from 'vue'
import Router from 'vue-router'
import login from '../views/login'
import home from '../layout/home.vue'
import venta from '../layout/venta.vue'
import ventavendedor from '../layout/venta.vendedor.vue'
import venta2 from '../layout/venta2.vue'
import dashboard from '../layout/dashboard.vue'
import axios from "axios";
//Catalogos
import Proveedor from '../views/catalogos/proveedor.vue'
import ProveedorCatalogo from '../views/catalogos/proveedor.catalogo.vue'
import Departamento from '../views/catalogos/departamento.vue'
import DepartamentoCatalogo from '../views/catalogos/departamento.catalogo.vue'
import categoria from '../views/catalogos/categoria.vue'
import CategoriaCatalogo from '../views/catalogos/categoria.catalogo.vue'
import Sucursal from '../views/catalogos/sucursal.vue'
import SucursalCatalogo from '../views/catalogos/sucursal.catalogo.vue'
import Articulo from '../views/catalogos/articulo.vue'
import ArticuloCatalogo from '../views/catalogos/articulo.catalogo.vue'
import Banco from '../views/catalogos/banco.vue'
import BancoCatalogo from '../views/catalogos/banco.catalogo.vue'
import Motivo from '../views/catalogos/motivos.vue'
import MotivoCatalogo from '../views/catalogos/motivo.catalogo.vue'

import usuario from '../views/catalogos/usuario.vue'
import usuarioNuevo from '../views/catalogos/usuario.nuevo.vue'
import Permisos from '../views/catalogos/permisos.vue'
import configuracionSistema from '../views/sistema/sistema.config.vue'

import cliente from '../views/catalogos/cliente.vue'
import clienteCatalogo from '../views/catalogos/cliente.catalogo.vue'
import reporteVentasSucursal from '../views/reportes/reporteVentasSucursal.vue'
import reporteResumenVenta from '../views/reportes/reporteResumenVenta.vue'
import reporteCortes from '../views/reportes/reporteCortes.vue'
import reporteSalidaEfectivo from '../views/reportes/reporteSalidaEfectivo.vue'
import reporteEntregaParcial from '../views/reportes/reporteEntregaParcial.vue'
import reporteInventario from '../views/reportes/reporteInventario.vue'
import reporteMovimientoArticulo from '../views/reportes/reporteMovimientoArticulo.vue'
import reporteTickets from '../views/reportes/reporteTickets.vue'
import reporteResurtido from '../views/reportes/reporteResurtido.vue'
import reporteRecepcion from '../views/reportes/reporteRecepcion.vue'

import ventas from '../views/ventas/ventas.vue'
import devolucionVentas from '../views/ventas/devoluciones.vue'
import devolucionNueva from '../views/ventas/devolucion.nueva.vue'

import entregaParcial from '../views/ventas/entregaParcial.vue'
import entregaParcialNuevo from '../views/ventas/entregaParcial.nuevo.vue'
import cierreVenta from '../views/ventas/cierreVenta.vue'
import cierreVentaNuevo from '../views/ventas/cierreVenta.nuevo.vue'
import salidaEfectivo from '../views/ventas/salidaEfectivo.vue'
import arqueo from '../views/ventas/arqueo.vue'

import salidaEfectivoNuevo from '../views/ventas/salidaEfectivo.nuevo.vue'
import tipoUsuario from '../views/catalogos/tipoUsuario.vue'
import tipoUsuarioNuevo from '../views/catalogos/tipoUsuario.catalogo.vue'
import unidad from '../views/catalogos/unidad.vue'
import unidadNuevo from '../views/catalogos/unidad.catalogo.vue'

import tipoSalida from '../views/catalogos/tipoSalida.vue'
import tipoSalidaNuevo from '../views/catalogos/tipoSalida.catalogo.vue'


import recepcion from '../views/procesos/recepcion.vue'
import recepcionNueva from '../views/procesos/recepcion.nuevo.vue'


import administracionMercancia from '../views/procesos/administracionMercancia.vue'
import  administracionmercanciaNuevo from '../views/procesos/administracionMercancia.nuevo.vue'


import caracteristica from '../views/catalogos/caracteristica.vue'
import caracteristicaNueva from '../views/catalogos/caracteristica.catalogo.vue'

import facturas from '../views/ventas/facturas.vue'
import reportefacturas from '../views/reportes/reporteFacturas.vue'
import reporteventacredito from '../views/reportes/reporteVentasCredito.vue'


import ventarapida from '../layout/ventaRapida.vue'
import abonos from '../views/ventas/abonos.vue'
import abonosNuevo from '../views/ventas/abonos.nuevo.vue'

import reporteArticulos from '../views/reportes/reporteArticulos.vue'
import vendedor from '../views/catalogos/vendedor.vue'
import vendedorNuevo from '../views/catalogos/vendedor.catalogo.vue'
import traspaso from '../views/procesos/traspaso.vue'
import traspasosucursal from '../views/procesos/traspaso.sucursal.vue'
import traspasovendedor from '../views/procesos/traspaso.vendedor.vue'
import reportetraspaso from '../views/reportes/reporteTraspaso.vue'
import inventariovendedor from '../views/reportes/reporteInventarioVendedor.vue'
import reporteClientes from '../views/reportes/reporteClientes.vue'
import reporteventascomisiones  from '../views/reportes/reporteVentasVendedor.vue'

Vue.use(Router)


const router = new Router(
{
  routes: [
    {
      path: '/',
      name: 'login',
      component: login
    },    
    {
      path: '/ventarapida',
      name: 'ventarapida',
      component: ventarapida
    },
    {
      path: '/home',
      name: 'home',
      component: home,
      children: [
        { path: '/dashboard', name: 'dashboard', component: dashboard },
        {
          path: '/venta',
          name: 'venta',
          component: venta
        },
        {
          path: '/ventavendedor',
          name: 'venta - vendedor',
          component: ventavendedor
        },
        
        // catalogos
        { path: '/proveedor', name: 'proveedor', component: Proveedor },
        { path: '/proveedor/nuevo', name: 'proveedorCatalogoNuevo', component: ProveedorCatalogo },
        { path: '/proveedor/edita/:id', name: 'proveedorCatalogoEdita', component: ProveedorCatalogo },
        { path: '/departamento', name: 'departamento', component: Departamento },
        { path: '/departamento/nuevo', name: 'departamentoCatalogoNuevo', component: DepartamentoCatalogo },
        { path: '/departamento/edita/:id', name: 'departamentoCatalogoEdita', component: DepartamentoCatalogo },
        { path: '/categoria', name: 'categoria', component: categoria },
        { path: '/categoria/nuevo', name: 'categoriaCatalogoNuevo', component: CategoriaCatalogo },
        { path: '/categoria/edita/:id', name: 'categoriaCatalogoEdita', component: CategoriaCatalogo },
        { path: '/sucursal', name: 'sucursal', component: Sucursal },
        { path: '/sucursal/nuevo', name: 'sucursalCatalogoNuevo', component: SucursalCatalogo },
        { path: '/sucursal/edita/:id', name: 'sucursalCatalogoEdita', component: SucursalCatalogo },
        { path: '/articulo', name: 'articulo', component: Articulo },
        { path: '/articulo/nuevo', name: 'articuloCatalogoNuevo', component: ArticuloCatalogo },
        { path: '/articulo/edita/:id', name: 'articuloCatalogoEdita', component: ArticuloCatalogo },
        { path: '/banco', name: 'banco', component: Banco },
        { path: '/banco/nuevo', name: 'bancoCatalogoNuevo', component: BancoCatalogo },
        { path: '/banco/edita/:id', name: 'bancoCatalogoEdita', component: BancoCatalogo },
        { path: '/motivo', name: 'motivo', component: Motivo },
        { path: '/motivo/nuevo', name: 'motivoCatalogoNuevo', component: MotivoCatalogo },
        { path: '/motivo/edita/:id', name: 'motivoCatalogoNuevo', component: MotivoCatalogo },
        { path: '/permisos', name: 'permisos', component: Permisos },
        { path: '/usuario', name: 'usuario', component: usuario },

        { path: '/tiposalida', name: 'tiposalida', component: tipoSalida },
        { path: '/tiposalida/nuevo', name: 'tiposalidaNuevo', component: tipoSalidaNuevo },
        { path: '/tiposalida/edita/:id', name: 'tiposalidaEdita', component: tipoSalidaNuevo },
        

        { path: '/tipoUsuario', name: 'tipoUsuario', component: tipoUsuario },
        { path: '/tipoUsuario/nuevo', name: 'tipoUsuarioNuevo', component: tipoUsuarioNuevo },
        { path: '/tipoUsuario/edita/:id', name: 'tipoUsuarioEdita', component: tipoUsuarioNuevo },
        { path: '/usuario/nuevo', name: 'usuarioNuevo', component: usuarioNuevo },
        { path: '/usuario/edita/:id', name: 'usuarioEdita', component: usuarioNuevo },
        { path: '/usuario', name: 'usuario', component: usuario },
        { path: '/unidad', name: 'unidad', component: unidad },
        { path: '/unidad/edita/:id', name: 'unidadEdita', component: unidadNuevo },
        { path: '/unidad/nuevo/', name: 'unidadNuevo', component: unidadNuevo },
        { path: '/configuracionsistema', name: 'configuracionsistema', component: configuracionSistema },     
            
        { path: '/cliente', name: 'cliente', component: cliente },
        { path: '/cliente/nuevo', name: 'clienteNuevo', component: clienteCatalogo },
        { path: '/cliente/edita/:id', name: 'clienteEdita', component: clienteCatalogo },
       
        { path: '/ventas', name: 'ventas', component: ventas },
        { path: '/devoluciones', name: 'devoluciones', component: devolucionVentas },
        { path: '/devolucion/nueva', name: 'devolucionNueva', component: devolucionNueva },
        
        { path: '/entregaparcial', name: 'entregaParcial', component: entregaParcial },
        { path: '/entregaparcial/nuevo', name: 'entregaParcialNuevo', component: entregaParcialNuevo },
        { path: '/entregaparcial/edita/:id', name: 'entregaParcialEdita', component: entregaParcialNuevo },
        { path: '/cierreventa', name: 'cierreVenta', component: cierreVenta },
        { path: '/cierreventa/nuevo', name: 'cierreVentaNuevo', component: cierreVentaNuevo },
        { path: '/cierreventa/edita/:id', name: 'cierreVentaEdita', component: cierreVentaNuevo },
        { path: '/salidaefectivo', name: 'salidaEfectivo', component: salidaEfectivo },
        { path: '/salidaefectivo/nuevo', name: 'salidaEfectivoNuevo', component: salidaEfectivoNuevo },
        { path: '/salidaefectivo/edita/:id', name: 'salidaEfectivoEdita', component: salidaEfectivoNuevo },
        { path: '/arqueo', name: 'arqueo', component: arqueo },
        { path: '/reporteVentas', name: 'reporteVentasSucursal', component: reporteVentasSucursal },        
        { path: '/resumenventas', name: 'resumenventas', component: reporteResumenVenta },
        { path: '/reportecortes', name: 'reporteCortes', component: reporteCortes },
        { path: '/reportesalidaefectivo', name: 'reporteSalidaEfectivo', component: reporteSalidaEfectivo },
        { path: '/reporteentregaparcial', name: 'reporteEntregaParcial', component: reporteEntregaParcial },
        { path: '/reporteinventario', name: 'reporteInventario', component: reporteInventario },
        { path: '/reportemovimientoarticulo', name: 'reporteMovimientoArticulo', component: reporteMovimientoArticulo },
        { path: '/reportemovimientoarticulo', name: 'reporteMovimientoArticulo', component: reporteMovimientoArticulo },
        { path: '/reporteTicket', name: 'reporteTicket', component: reporteTickets },
        { path: '/reporteResurtido', name: 'reporteresurtido', component: reporteResurtido },
        { path: '/reporteRecepcion', name: 'reporterecepcion', component: reporteRecepcion },
        { path: '/reportefacturas', name: 'reportefacturas', component: reportefacturas },
        
        { path: '/recepcion', name: 'recepcion', component: recepcion },
        { path: '/recepcion/nuevo', name: 'recepcionNuevo', component: recepcionNueva },
        { path: '/recepcion/consulta/:id', name: 'recepcionconsulta', component: recepcionNueva },

        { path: '/gestioninventario', name: 'gestioninventario', component: administracionMercancia },
        { path: '/gestioninventario/nuevo', name: 'gestioninventarionuevo', component: administracionmercanciaNuevo },
        { path: '/caracteristica', name: 'caracteristica', component: caracteristica },
        { path: '/caracteristica/nuevo', name: 'caracteristicaNueva', component: caracteristicaNueva },
        { path: '/caracteristica/edita/:id', name: 'caracteristicaNueva', component: caracteristicaNueva },
        { path: '/facturas', name: 'facturas', component: facturas },
        { path: '/abonos', name: 'abonos', component: abonos },
        { path: '/abono/nuevo', name: 'abonosnuevo', component: abonosNuevo },
        { path: '/reporteventacredito', name: 'reporteventacredito', component: reporteventacredito },
        { path: '/reportearticulos', name: 'reporteArticulos', component: reporteArticulos },
        { path: '/vendedores', name: 'vendedores', component: vendedor },
        { path: '/vendedor/edita/:id', name: 'vendedorEdita', component: vendedorNuevo },
        { path: '/vendedor/nuevo/', name: 'vendedorNuevo', component: vendedorNuevo },
        { path: '/traspaso', name: 'traspaso', component: traspaso },

        { path: '/traspaso/sucursal', name: 'traspasosucursal', component: traspasosucursal },
        { path: '/traspaso/vendedor', name: 'traspasovendedor', component: traspasovendedor },
        { path: '/traspaso/vendedor/:id', name: 'recepcionconsulta', component: traspasovendedor },
        { path: '/reportetraspaso', name: 'reportetraspaso', component: reportetraspaso },
        { path: '/inventariovendedor', name: 'inventariovendedor', component: inventariovendedor },
        { path: '/reporteclientes', name: 'reporteclientes', component: reporteClientes },
        { path: '/reporteventascomisiones', name: 'reporteventascomisiones', component: reporteventascomisiones },
        
        
      ]
    }
  ]
})


/* router.beforeEach((to, from, next) => {
  console.log(to)
  console.log(from)

  if(to.name == 'login'){
    next();
    return
  }
  if(to.name == 'dashboard'){
    next();
    return
  }
 
   let body = {
    tipoUsuario: Vue.localStorage.get("USER_TYPE"),
    op:1
  };
  let  header= {
    headers: {
      Authorization: "Basic " + Vue.localStorage.get("ACCESS_TOKEN")
    }
  }
  axios
    .post("/Usuario/GetObtenModulosUsuario", body, header)
    .then(res => {
      let todos = []      
      let menu = res.data.GetObtenModulosUsuarioResult
      menu.forEach(i=> i.SubModulos.forEach(f=> todos.push(f)) )
      console.log(todos.some(el => el.componente == to.name))
      console.log(todos)
      if(todos.some(el => el.componente == to.name)){
        next();
      }else{
        next({ path: '/dashboard' });
      }
      
    });
  }); */


export default router