const axios = require('axios')
async function GetUsuario (token) {
  try {
    let resp = await http.get('/Usuario/GetUsuario')
    return resp.data.GetUsuarioResult
  } catch (e) {}
}
async function GetUsuarioById (token, id) {
  try {
    let user = await http.get(`/Usuario/GetUsuarioById?id=${id}`)
    return user.data.GetUsuarioByIdResult
  } catch (e) {}
}

async function AddUsuario (token, usuario, sucursales) {
  try {
    let data = {usuario: usuario, sucursales: sucursales}
    let resp = await http.post( `/Usuario/AddUsuario`, data)
    // notyf.confirm('El usuario se ha guardado correctamente')
    return resp
  } catch (e) {}
}
async function UpdateUsuario (token, usuario, sucursales) {
  try {
    let data = {usuario: usuario, sucursales: sucursales}
    let resp = await http.post('Usuario/UpdateUsuario', data)
    // notyf.confirm('usuario Editado  correctamente')
    return resp
  } catch (e) {}
}

async function GetSucursalUsuario (token, id) {
  try {
    let resp = await http.get(`/Usuario/GetSucursalUsuario?id=${id}`)
    return resp.data.GetSucursalUsuarioResult
  } catch (e) {}
}

export default {
  GetUsuario: GetUsuario,
  GetUsuarioById: GetUsuarioById,
  AddUsuario: AddUsuario,
  UpdateUsuario: UpdateUsuario,
  GetSucursalUsuario: GetSucursalUsuario
}
