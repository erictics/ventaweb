// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import Snotify, { SnotifyPosition } from 'vue-snotify'
import es from 'vee-validate/dist/locale/es'
import VueLocalStorage from 'vue-localstorage'
import VeeValidate, { Validator } from 'vee-validate'
import VueProgressBar from 'vue-progressbar'
import BlockUI from 'vue-blockui'
import VModal from 'vue-js-modal'
import 'vue-js-modal/dist/styles.css'

import VueCurrencyFilter from 'vue-currency-filter'
import moment from 'moment'
import global from './globals'
import VueTabs from 'vue-nav-tabs'
import 'vue-nav-tabs/themes/vue-tabs.css'


import '@morioh/v-lightbox/dist/lightbox.css';
import Lightbox from '@morioh/v-lightbox'
import VueSlideoutPanel from 'vue2-slideout-panel';
import * as VueGoogleMaps from 'vue2-google-maps'





Vue.use(VueSlideoutPanel);
// global register
Vue.use(Lightbox);
moment.locale('es')
Vue.use(moment)
Vue.use(VueTabs)
Vue.use(VModal, { })
Vue.use(BlockUI)
const options = {
  toast: {
    position: SnotifyPosition.rightTop
  }
}
Vue.use(VueCurrencyFilter,
  {
    symbol: '$',
    thousandsSeparator: ',',
    fractionCount: 2,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: true
  })
Vue.use(Snotify, options)
Vue.use(VeeValidate)
Vue.use(VueLocalStorage)
axios.defaults.baseURL = global.URL_SERVICE
axios.defaults.headers.post['Content-Type'] = 'application/json'
Validator.localize('es', es)
Vue.config.productionTip = false
const config = {
  errorBagName: 'errors', // change if property conflicts
  fieldsBagName: 'fields',
  delay: 0,
  locale: 'es',
  dictionary: null,
  strict: true,
  classes: false,
  classNames: {
    touched: 'touched', // the control has been blurred
    untouched: 'untouched', // the control hasn't been blurred
    valid: 'valid', // model is valid
    invalid: 'invalid', // model is invalid
    pristine: 'pristine', // control has not been interacted with
    dirty: 'dirty' // control has been interacted with
  },
  events: 'input|blur',
  inject: true,
  validity: false,
  aria: true,
  i18n: null, // the vue-i18n plugin instance,
  i18nRootKey: 'validations' // the nested key under which the validation messsages will be located
}

Vue.use(VeeValidate, config)
const options2 = {
  color: '#2196f3',
  failedColor: '#874b4b',
  thickness: '5px',
  transition: {
    speed: '0.2s',
    opacity: '0.6s',
    termination: 300
  },
  autoRevert: true,
  location: 'top',
  inverse: false
}

Vue.use(VueProgressBar, options2)


//import VueGoogleMaps   from 'vue2-google-maps'
 
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyD2lhDWhZmr9BfnXtrblnexM8R8CouY9WE',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
 
    //// If you want to set the version, you can do so:
    // v: '3.26',
  },
 
  //// If you intend to programmatically custom event listener code
  //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  //// you might need to turn this on.
   autobindAllEvents: true,
 
  //// If you want to manually install components, e.g.
  //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  //// Vue.component('GmapMarker', GmapMarker)
  //// then disable the following:
  // installComponents: true,
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
