
const URL_SERVICE = 'http://20.234.236.181/AppWCFService.svc'
const URL_REPORTS = 'http://20.234.236.181/Reportes/'
const URL_DOCUMENTOS = 'http://20.234.236.181/AppWCFService.svc'
const URL_DOCUMENTOS_TAREAS = 'http://20.234.236.181/tareas-docs/'
const URL_DOCUMENTOS_ARTICULOS = 'http://20.234.236.181/articulos-docs/'
const URL_DOCUMENTOS_CLIENTES = 'http://20.234.236.181/clientes-docs/'
const URL_IMAGE_LOGOS = 'http://20.234.236.181/logos/'
const VERSION = '1.0'



function createSession (Vue, user) {
  Vue.localStorage.set('USER_ID', user.id)
  Vue.localStorage.set('USER_NAME', user.nombre)
  Vue.localStorage.set('USER_EMAIL', user.email)
  Vue.localStorage.set('USER_TYPE', user.RoltipoUsuario.Id)
  Vue.localStorage.set('ACCESS_TOKEN', user.token)
  Vue.localStorage.set('USER_CANCELA_VENTA', user.RoltipoUsuario.CancelaVenta)
  Vue.localStorage.set('USER_REIMPRIME_VENTA', user.RoltipoUsuario.ReimprimeTicket)
}

function ClearSession (Vue,user) {
  Vue.localStorage.remove('USER_ID')
  Vue.localStorage.remove('USER_NAME')
  Vue.localStorage.remove('USER_EMAIL')
  Vue.localStorage.remove('USER_TYPE')
  Vue.localStorage.remove('ACCESS_TOKEN')
  Vue.localStorage.remove('USER_SUCURSAL')
  Vue.localStorage.remove('SUCURSAL')
  Vue.localStorage.remove('USER_CANCELA_VENTA')
  Vue.localStorage.remove('USER_REIMPRIME_VENTA')
}




module.exports = {
  URL_SERVICE,
  URL_REPORTS,
  URL_DOCUMENTOS,
  URL_DOCUMENTOS_TAREAS,
  URL_IMAGE_LOGOS,
  URL_DOCUMENTOS_ARTICULOS,
  URL_DOCUMENTOS_CLIENTES,
  VERSION,
  createSession,
  ClearSession
}
